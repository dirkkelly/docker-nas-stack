# Docker NAS Stack

## Overview

This stack is designed to get you up and running as quickly as possible with a fully-fledged multimedia stack. You'll need to provide accounts for everything, obviously.

## Installation

### General

Copy `.env.example` to `.env` and edit all appropriate variables.

If you don't want/need services (e.g. `ddclient` if you have a static IP), comment them out of `docker-compose.yml`.

### Traefik

Edit `traefik.toml` and replace the values for `docker.domain`, `acme.email`.

### Transmission

1. Retrieve your `.ovpn` file from your VPN provider and place it in the `transmission-openvpn` folder, named `vpn.ovpn`.
2. Create a file in the `transmission-openvpn` folder called `auth.vpn` and paste your username and password in place of the existing `username` and `password` strings.
3. Edit the `docker-compose.yml` file if required to change default environment variables (e.g. ratio limts, queue sizes).

### DDClient

1. Edit `ddclient/ddclient.conf` with your appropriate details.

## Starting

> docker-compose up -d

Watchtower will ensure that images are kept up-to-date (where they're tagged to `latest`) and autoheal will ensure that they restart if they become unresponsive.

## Configuration

Once started, you'll need to configure services - Traefik will use a self-signed TLS certificate until your domain is propagated and it's picked one up from LetsEncrypt, so you can access the services with `https` using your browser.

When configuring the services, there's something to keep in mind: this is Docker, so while you're accessing everything from a reverse proxy, the services should access each other using internal hostnames. The internal hostname to access a service is generally the service name (e.g. `plex` or `sonarr`), so when linking Sonarr to NzbGet you'd generally give it a hostname of `nzbget` and the default port (`6789`).

This is really just a helper compose - edit it to suit your needs!